# AOUtils

[![CI Status](http://img.shields.io/travis/Aymeric De Abreu/AOUtils.svg?style=flat)](https://travis-ci.org/Aymeric De Abreu/AOUtils)
[![Version](https://img.shields.io/cocoapods/v/AOUtils.svg?style=flat)](http://cocoapods.org/pods/AOUtils)
[![License](https://img.shields.io/cocoapods/l/AOUtils.svg?style=flat)](http://cocoapods.org/pods/AOUtils)
[![Platform](https://img.shields.io/cocoapods/p/AOUtils.svg?style=flat)](http://cocoapods.org/pods/AOUtils)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

AOUtils is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "AOUtils"
```

## Author

Flavien Simon, flavien@appouest.com
Jean-Baptiste Denoual, jbd@appouest.com
Aymeric De Abreu, ada@appouest.com

## License

AOUtils is available under the MIT license. See the LICENSE file for more info.

##Test your specs : Inside directory where AOUtils.podspec is.
pod spec lint AOUtils.podspec --verbose --allow-warnings
##Push your specs : Inside directory where AOUtils.podspec is.
pod repo push bitbucket-appouest-specs AOUtils.podspec --allow-warnings
