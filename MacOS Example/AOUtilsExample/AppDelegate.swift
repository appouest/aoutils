//
//  AppDelegate.swift
//  AOUtilsExample
//
//  Created by Flavien Simon on 17/08/2017.
//  Copyright © 2017 Flavien Simon. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

