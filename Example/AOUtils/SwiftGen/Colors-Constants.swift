// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen
import AOUtils

#if os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIColor
typealias Color = UIColor
#elseif os(OSX)
import AppKit.NSColor
typealias Color = NSColor
#endif

// swiftlint:disable operator_usage_whitespace
extension Color {
convenience init(rgbaValue: UInt32) {
let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0

self.init(red: red, green: green, blue: blue, alpha: alpha)
}
}
// swiftlint:enable operator_usage_whitespace

// swiftlint:disable file_length
// swiftlint:disable line_length

// swiftlint:disable type_body_length
enum ColorName: String {
case blue  = "blue"
case green  = "green"
case red  = "red"

var rgbaValue: UInt32 {
switch self {
case .blue:
return 0x0000ffff
case .green:
return 0x00ff00ff
case .red:
return 0xff0000ff
}
}

var color: Color {
return Color(named: self)
}
}
// swiftlint:enable type_body_length

extension Color {
convenience init(named name: ColorName) {
self.init(rgbaValue: name.rgbaValue)
}
}
// swiftlint:enable file_length
// swiftlint:enable line_length

// SwiftGen Inspectables

public class ColorUtils {
static var colorFromString : (String) -> Color = {

return { colorStr in

var color = Color.black
if let colorName = ColorName(rawValue: colorStr)
{
color = colorName.color
}
return color
}
}()
}

#if os(iOS) || os(tvOS) || os(watchOS)

extension UILabel {

@IBInspectable var textColorGen: String {
get {
return text ?? ""
}
set (value) {
textColor = ColorUtils.colorFromString(value)
}
}
}

extension UITextField {

@IBInspectable var textColorGen: String {
get {
return text ?? ""
}
set (value) {

textColor = ColorUtils.colorFromString(value)

}
}

}

extension UITextView {

@IBInspectable var textColorGen: String {
get {
return text ?? ""
}
set (value) {

textColor = ColorUtils.colorFromString(value)

}
}
}


extension UINavigationBar {
@IBInspectable var barTintColorGen: String {
get {
return ""
}
set (value) {
barTintColor = ColorUtils.colorFromString(value)
}
}
}

extension UIView {

@IBInspectable var borderColorGen: String {
get {
return ""
}
set (value) {

layer.borderColor = ColorUtils.colorFromString(value).cgColor

}
}

@IBInspectable var shadowColorGen: String {
get {
return ""
}
set (value) {

layer.shadowColor = ColorUtils.colorFromString(value).cgColor

}
}

@IBInspectable var backgroundColorGen: String {
get {
return ""
}
set (value) {

backgroundColor = ColorUtils.colorFromString(value)

}
}

@IBInspectable var tintColorGen: String {
get {
return ""
}
set (value) {

tintColor = ColorUtils.colorFromString(value)

}
}
}

extension UIButton {

@IBInspectable var titleColorGen: String {
get {
return ""
}
set (value) {

setTitleColor(ColorUtils.colorFromString(value), for: .normal)

}
}
}

extension UIBarButtonItem {

@IBInspectable var tintColorGen: String {
set (value) {
tintColor = ColorUtils.colorFromString(value)
}
get {
return ""
}
}
}

extension UISwitch {

@IBInspectable var onColorGen: String {
set (value) {
onTintColor = ColorUtils.colorFromString(value)
}
get {
return ""
}
}

}


extension UISlider {

@IBInspectable var minColorGen: String? {
set (key) {

if let key = key
{
minimumTrackTintColor = ColorUtils.colorFromString(key)
}

}
get {
return nil
}
}

@IBInspectable var maxColorGen: String? {
set (key) {

if let key = key
{
maximumTrackTintColor = ColorUtils.colorFromString(key)
}

}
get {
return nil
}
}

}

extension UITableView
{
@IBInspectable var separatorColorGen: String? {
set (key)
{
if let key = key
{
separatorColor = ColorUtils.colorFromString(key)
}
}
get {
return nil
}
}

}

#endif


