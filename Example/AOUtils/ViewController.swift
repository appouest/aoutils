//
//  ViewController.swift
//  AOUtils
//
//  Created by Aymeric De Abreu on 02/13/2017.
//  Copyright (c) 2017 Aymeric De Abreu. All rights reserved.
//

//#define RX 0
//#define PROMISE 0

import UIKit
#if RX
import RxSwift
#endif
#if PROMISE
import PromiseKit
#endif
import AOUtils
import SwiftyJSON

enum GithubResource {
    case repo(owner:String, name: String)
}

enum SlackUpload {
    case upload(data: Data)
    case post
}

struct GithubResult : CustomStringConvertible {
    let name: String
    let owner: String
    let htmlUrl: String
    let desc: String
    let numberOfStars: Int
    let createdAt: Date
    let updatedAt: Date
    
    init(withJSON json: JSON) {
        name = json["name"].stringValue
        owner = json["onwer"]["login"].stringValue
        htmlUrl = json["html_url"].stringValue
        desc = json["description"].stringValue
        numberOfStars = json["watchers"].intValue
        if #available(iOS 10.0, *) {
            let dateFormatter = ISO8601DateFormatter()
            dateFormatter.timeZone = TimeZone.current
            
            if let d = dateFormatter.date(from: json["created_at"].stringValue) {
                createdAt = d
            } else {
                createdAt = Date()
            }
            
            if let d = dateFormatter.date(from: json["updated_at"].stringValue) {
                updatedAt = d
            } else {
                updatedAt = Date()
            }
            
        } else {
            updatedAt = Date()
            createdAt = Date()
        }
        
        
    }
    
    var description: String {
        return "Name : \(name)\nOwner : \(owner)\nHtml Url : \(htmlUrl)\nDescription : \(desc)\nNumber of stars : \(numberOfStars)\nCreated at : \(createdAt)\nUpdated At : \(updatedAt)"
    }
}
// xoxp-9297065301-9296555316-396206581345-cba47704206753c630d281d997038154

extension SlackUpload: Resource {
    var path: String {
        switch self {
        case .upload:
            return "https://slack.com/api/files.upload"
        case .post:
            return "https://slack.com/api/chat.postMessage"
        }
        
    }
    
    var method: AOUtils.Method {
        return .POST
    }
    
    var parameters: [String: Any]? {
        var params = [
            "token": "xoxp-9297065301-9296555316-396206581345-cba47704206753c630d281d997038154",
            "channels": "CBNNNB36E"
        ]
        switch self {
        case .post:
            params["channel"] = params["channels"]
            params["username"] = "AOUtils Example"
            params["text"] = "Test d'envoi POST via AOUtils éàè"
        default: break
        }
        return params
    }
    
    var multiparts: [String : Any]? {
        switch self {
        case .upload(let data):
            return [
                "file": MultipartData(filename: "test", data: data)
            ]
        default: return nil
        }
        
    }
}

extension GithubResource : Resource {
    var path: String {
        switch self {
        case .repo(let owner, let name):
            return "/repos/\(owner)/\(name)"
        }
    }
    
    var needAuth: Bool {
        return false
    }
    
    var parameters: [String : Any]? {
        return ["test" : [23, 54, 54]]
    }
    #if RX
    static func parse(_ json:JSON) -> Observable<GithubResult> {
        return Observable.create({ observer in
            
            let data = json
            if data != .null {
                observer.onNext(GithubResult(withJSON: data))
            }
            
            observer.onCompleted()
            return Disposables.create()
        })
    }
    #endif
    #if PROMISE
    static func parse(_ json:JSON) -> Promise<GithubResult> {
        return Promise { seal in

            let data = json
            if data != .null {
                seal.fulfill(GithubResult(withJSON: data))
            }
            else
            {
                seal.reject(NSError(domain: "com.appouest.aoutils", code: 123, userInfo: [NSLocalizedDescriptionKey: "Should not happen"]))
            }
        }
    }
    #endif
    #if RESULT
    static func parse(_ json:JSON) -> Result<GithubResult, Error> {
        let data = json
        if data != .null {
            return Result.success(GithubResult(withJSON: data))
        }
        else
        {
            return Result.failure(NSError(domain: "com.appouest.aoutils", code: 123, userInfo: [NSLocalizedDescriptionKey: "Should not happen"]))
        }
    }
    #endif


    
}

struct LogRequestAdapter: RequestAdapter {
    
    func adapt(_ request: inout URLRequest) {
        if let url = request.url {
            print("[RequestLog] Adapt \(url)")
        } else {
            print("[RequestLog] Adapt \(request)")
        }
    }
    
    func beforeSend(_ request: URLRequest) {
        if let url = request.url {
            print("[RequestLog] Before Send \(url)")
        } else {
            print("[RequestLog] Before Send \(request)")
        }
    }
    
    func onResponse(response: URLResponse?, data: Data?) {
        
        if let httpResponse = response as? HTTPURLResponse {
            print("[RequestLog] onResponse status : \(httpResponse.statusCode)")
        } else if let response = response {
            print("[RequestLog] onResponse \(response)")
        }
    }
    
    func onError(request: URLRequest?, error: Error) {
        if let url = request?.url {
            print("[RequestLog] onError \(url) : \(error)")
        } else {
            print("[RequestLog] onError \(request) : \(error)")
        }
    }
    
    func onSuccess(request: URLRequest?) {
        if let url = request?.url {
            print("[RequestLog] Success \(url)")
        } else {
            print("[RequestLog] Success \(request)")
        }
    }
    
}

class ViewController: UIViewController {
    #if RX
    let disposeBag = DisposeBag()
    #endif
    
    @IBOutlet var imageView: UIImageView?

    override func viewDidLoad() {
        super.viewDidLoad()
        APIClient.url = URL(string:"https://api.github.com/")
        
        #if RX
        APIClient.shared.data(resource: GithubResource.repo(owner: "apple", name: "swift"))
            .flatMap({ JSON.toJSON(data: $0) })
            .flatMap({ GithubResource.parse($0) })
            .subscribe(onNext: { result in
                print(lvl:.info, "Result Github : \(result)")
            }, onError: { (error) in
                print(lvl:.error, "Error : \(error)")
            }, onCompleted: { 
                print(lvl:.info, "OnCompleted")
            }).disposed(by: disposeBag)
        #endif
        
        #if PROMISE
        Request.shared = Request(configuration: URLSessionConfiguration.background(withIdentifier: "test"),
            adapters: [LogRequestAdapter()])
        APIClient.shared.data(resource: GithubResource.repo(owner: "apple", name: "swift"))
            .then { JSON.toJSON(data: $0) }
            .then { GithubResource.parse($0) }
            .done { print(lvl: .info, "result : \n\($0)") }
            .catch { error in print(lvl: .error, "Error : \(error)") }
        
        SlackUpload.post.request()
            .done { print("POST Success : \(String(data: $0, encoding: .utf8) ?? "")") }
            .catch { print("error while posting : \($0)") }
        
        Request.shared.get(url: URL(string: "https://source.unsplash.com/random")!,
                           parameters: ["useless": "coucou"])
            .then { data -> Promise<Data> in
                if let image = UIImage(data: data) {
                    self.imageView?.image = image
                }
                return SlackUpload.upload(data: data).request(progress: { type, prog in
                    print("\(type) progress : \(prog * 100)%")
                })
            }
            .done { _ in
                print("upload success & fetching image")
            }
            .catch { print("error fetching image or while uploading : \($0)") }
        #endif
        #if RESULT
        APIClient.shared.data(resource: GithubResource.repo(owner: "apple", name: "swift")) {
            do {
                let result = try $0.flatMap { JSON.toJSON(data: $0) }
                    .flatMap { GithubResource.parse($0) }
                    .get()
                print(lvl: .info, "result : \n\(result)")
            } catch {
                print(lvl: .error, "Error : \(error)")
            }
        }
        
        SlackUpload.post.request(completion: {
            do {
                let result = try $0.get()
                print("POST Success : \(String(data: result, encoding: .utf8) ?? "")") 
            } catch {
                print("error while posting : \(error)")
            }
        })
        #endif
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

