//
//  SKProduct+Extension.swift
//  Ana
//
//  Created by Flavien Simon on 09/12/2016.
//  Copyright © 2016 AppOuest. All rights reserved.
//

import StoreKit

extension SKProduct {
    
    public func localizedPrice() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = self.priceLocale
        return formatter.string(from: self.price) ?? ""
    }
    
}
