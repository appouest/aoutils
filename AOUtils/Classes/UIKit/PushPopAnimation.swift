//
//  PushPopAnimation.swift

//
//  Created by Aymeric De Abreu on 02/01/2017.
//  Copyright © 2017 Aymeric De Abreu. All rights reserved.
//

import Foundation
import UIKit

public class PushAnimator : NSObject, UIViewControllerAnimatedTransitioning {
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval
    {
        return 0.3
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        guard let toVC = transitionContext.viewController(forKey: .to),
            let fromVC = transitionContext.viewController(forKey: .from) else {
                return
        }
        
        let container = transitionContext.containerView
        toVC.view.frame = container.bounds
        toVC.view.frame.origin.x += container.bounds.width
        container.addSubview(toVC.view)
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: { 
            toVC.view.frame.origin.x = 0
            fromVC.view.frame.origin.x = -fromVC.view.bounds.width
        }) { (completed) in
            transitionContext.completeTransition(completed)
        }
    }
}

public class PopAnimator : NSObject, UIViewControllerAnimatedTransitioning {
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval
    {
        return 0.25
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        guard let toVC = transitionContext.viewController(forKey: .to),
            let fromVC = transitionContext.viewController(forKey: .from) else {
                return
        }
        
        let container = transitionContext.containerView
        toVC.view.frame = container.bounds
        toVC.view.frame.origin.x -= container.bounds.width
        container.addSubview(toVC.view)
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            toVC.view.frame.origin.x = 0
            fromVC.view.frame.origin.x = fromVC.view.bounds.width
        }) { (completed) in
            transitionContext.completeTransition(completed)
        }

    }
}

