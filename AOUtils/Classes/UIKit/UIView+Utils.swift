//
//  UIView+Tools.swift
//  Pods
//
//  Created by Jean-Baptiste Denoual on 10/08/2017.
//
//

import Foundation

extension UIView {
    
    public func fillParent()
    {
        if let superview = superview {
            
            translatesAutoresizingMaskIntoConstraints = false
            topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
            bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
            leadingAnchor.constraint(equalTo: superview.leadingAnchor).isActive = true
            trailingAnchor.constraint(equalTo: superview.trailingAnchor).isActive = true
        }
    }
    
    public func snapshotImage() -> UIImage? {
        let color = layer.backgroundColor
        layer.backgroundColor = nil
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let snapshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        layer.backgroundColor = color
        return snapshotImage
    }
    
    public func snapshotView() -> UIView? {
        
        if let snapshotImage = snapshotImage() {
            return UIImageView(image: snapshotImage)
        } else {
            return nil
        }
    }
}
