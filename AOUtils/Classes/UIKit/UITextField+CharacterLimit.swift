//
//  UITextField+CharacterLimit.swift
//  Barns
//
//  Created by Jean-Baptiste Denoual on 09/11/2017.
//  Copyright © 2017 AppOuest. All rights reserved.
//

import Foundation
import UIKit

private var __maxLengths = [UITextField: Int]()
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return Int.max
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }
}

extension String
{
    func safelyLimitedTo(length n: Int)->String {
        if (count <= n) { return self }
        return String(self[self.index(self.startIndex, offsetBy: n)...])
    }
    
    func index(position: Int) -> String.Index {
        return self.index(self.startIndex, offsetBy: position)
    }
}
