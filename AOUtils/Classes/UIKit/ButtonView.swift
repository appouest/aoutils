//
//  ButtonView.swift
//  Alphorm
//
//  Created by Aymeric De Abreu on 23/01/2017.
//  Copyright © 2017 Aymeric De Abreu. All rights reserved.
//

import UIKit


open class ButtonView : UIControl {
    
    var overlayView: UIView?
    var needsMaskGeneration = true
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit()
    {
        overlayView = UIView(frame:bounds)
        overlayView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        overlayView?.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        addSubview(overlayView!)
        overlayView?.isHidden = true
        
        addTarget(self, action: #selector(ButtonView.touchDown), for: .touchDown)
        addTarget(self, action: #selector(ButtonView.touchUp), for: .touchUpInside)
        addTarget(self, action: #selector(ButtonView.touchUp), for: .touchCancel)
        addTarget(self, action: #selector(ButtonView.touchUp), for: .touchDragExit)
    }
    
    @IBInspectable public var darkContent: Bool = false {
        didSet {
            overlayView?.backgroundColor = (darkContent) ? UIColor.white.withAlphaComponent(0.6) : UIColor.black.withAlphaComponent(0.6)
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        needsMaskGeneration = true
    }
    
    public func prepareForReuse() {
        needsMaskGeneration = true
    }
    
    @objc func touchDown()
    {
        if let overlay = overlayView {
            bringSubviewToFront(overlay)
        }
        if needsMaskGeneration {
            needsMaskGeneration = false
            let mask = snapshotView()
            overlayView?.mask = mask
        }
        overlayView?.isHidden = false
    }
    
    @objc func touchUp()
    {
        overlayView?.isHidden = true
    }
    
    public override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        return bounds.contains(point) ? self : nil
    }

}

