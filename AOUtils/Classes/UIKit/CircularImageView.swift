//
//  CircularImageView.swift

//
//  Created by Aymeric De Abreu on 26/12/2016.
//  Copyright © 2016 Aymeric De Abreu. All rights reserved.
//

import UIKit

open class CircularImageView: UIImageView {

    public override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.width / 2.0
        clipsToBounds = true
    }
}
