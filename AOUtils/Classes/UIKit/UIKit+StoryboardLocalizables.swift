//
//  Localization.swift
//  Pods
//
//  Created by Aymeric De Abreu on 13/02/2017.
//
//

import Foundation
import UIKit

extension UILabel {
    @IBInspectable var localizedText: String {
        get {
            return text ?? ""
        }
        set (value) {
            text = NSLocalizedString(value, comment: "")
        }
    }
}

extension UITextField {
    
    @IBInspectable var localizedText: String {
        get {
            return text ?? ""
        }
        set (value) {
            text = NSLocalizedString(value, comment: "")
        }
    }
    
    @IBInspectable var localizedPlaceholder: String {
        get {
            return placeholder ?? ""
        }
        set (value) {
            placeholder = NSLocalizedString(value, comment: "")
        }
    }
}

extension UITextView {
    
    @IBInspectable var localizedText: String {
        get {
            return text ?? ""
        }
        set (value) {
            text = NSLocalizedString(value, comment: "")
        }
    }
    
}

extension UIButton
{
    @IBInspectable var localizedTitle: String {
        get {
            return title(for: .normal) ?? ""
        }
        set (value) {
            setTitle(NSLocalizedString(value, comment: ""), for: .normal)
        }
    }
}

extension UIBarButtonItem
{
    @IBInspectable var localizedTitle: String {
        get {
            return title ?? ""
        }
        set (value) {
            title = NSLocalizedString(value, comment: "")
        }
    }
}

extension UITabBarItem {
    @IBInspectable var localizedTitle: String {
        get {
            return title ?? ""
        }
        set (value) {
            
            
            title = NSLocalizedString(value, comment: "")
        }
    }
}

extension UIViewController {
    @IBInspectable var localizedTitle: String {
        get {
            return title ?? ""
        }
        set (value) {
            title = NSLocalizedString(value, comment: "")
        }
    }
    
    @IBInspectable var localizedBackTitle: String {
        get {
            return navigationItem.backBarButtonItem?.title ?? ""
        }
        set (value) {
            navigationItem.backBarButtonItem = UIBarButtonItem(title: NSLocalizedString(value, comment: ""), style: .done, target: nil, action: nil)
        }
    }
}
