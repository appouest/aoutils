//
//  GradientView.swift
//  Alphorm
//
//  Created by Jean-Baptiste on 25/01/2017.
//  Copyright © 2017 Aymeric De Abreu. All rights reserved.
//

import UIKit

open class GradientView: UIView {
    
    var gradientLayer: CAGradientLayer?
    
    @IBInspectable public var isHorizontal: Bool = false {
        didSet {
            updateColors()
        }
    }
    
    @IBInspectable public var gradientStartColor: UIColor = .white {
        didSet {
            updateColors()
        }
    }
    
    @IBInspectable public var gradientEndColor: UIColor = .black {
        didSet {
            updateColors()
        }
    }
    
    func updateColors()
    {
        if gradientLayer == nil {
            gradientLayer = CAGradientLayer()
            layer.masksToBounds = true
        }
        gradientLayer?.frame = bounds
        gradientLayer?.colors = [gradientStartColor.cgColor, gradientEndColor.cgColor]
        
        if isHorizontal {
            gradientLayer?.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradientLayer?.endPoint = CGPoint(x: 1.0, y: 0.5)
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        updateColors()
        layer.insertSublayer(gradientLayer!, at: 0)
    }
    
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit()
    {
        
        updateColors()
    }

}
