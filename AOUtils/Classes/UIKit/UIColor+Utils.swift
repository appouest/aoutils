//
//  UIColor+Extension.swift
//  Pods
//
//  Created by Flavien Simon on 06/07/2017.
//
//

import UIKit

extension UIColor {
    
    public convenience init(red: Int, green: Int, blue: Int) {
        
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    public static func gradient(leftColor: UIColor, rightColor: UIColor) -> CAGradientLayer
    {
        let gl = CAGradientLayer()
        gl.colors = [leftColor.cgColor, rightColor.cgColor]
        gl.startPoint = CGPoint(x: 0.0, y: 0.5)
        gl.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        return gl
    }
    
    public static func gradient(topColor: UIColor, bottomColor: UIColor) -> CAGradientLayer
    {
        let gl = CAGradientLayer()
        gl.colors = [topColor.cgColor, bottomColor.cgColor]
        gl.startPoint = CGPoint(x: 0.5, y: 0)
        gl.endPoint = CGPoint(x: 0.5, y: 1.0)
        
        return gl
    }
    
    public convenience init(netHex:Int)
    {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    
    public convenience init(hexString: String) {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexString)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        self.init(netHex:Int(hexInt))
    }
    
    public func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return String(format:"#%06x", rgb)
    }
    
    public static func morph(from: UIColor, to: UIColor, percent: Double) -> UIColor
    {
        var fromRed : CGFloat = 0.0
        var fromGreen : CGFloat = 0.0
        var fromBlue : CGFloat = 0.0
        
        var toRed : CGFloat = 0.0
        var toGreen : CGFloat = 0.0
        var toBlue : CGFloat = 0.0
        
        from.getRed(&fromRed, green: &fromGreen, blue: &fromBlue, alpha: nil)
        to.getRed(&toRed, green: &toGreen, blue: &toBlue, alpha: nil)
        
        let dR = toRed - fromRed
        let dG = toGreen - fromGreen
        let dB = toBlue - fromBlue
        
        let resultRed = fromRed + dR * CGFloat(percent/100)
        let resultGreen = fromGreen + dG * CGFloat(percent/100)
        let resultBlue = fromBlue + dB * CGFloat(percent/100)
        
        return UIColor(red: resultRed, green: resultGreen, blue: resultBlue, alpha: 1.0)
    }
}
