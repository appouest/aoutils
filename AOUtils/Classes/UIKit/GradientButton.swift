//
//  GradientButton.swift
//  Alphorm
//
//  Created by Aymeric De Abreu on 23/01/2017.
//  Copyright © 2017 Aymeric De Abreu. All rights reserved.
//

import UIKit


open class GradientButton: UIButton {
    
    var gradientLayer: CAGradientLayer?
    var overlayView: UIView?
    
    @IBInspectable public var isHorizontal: Bool = false {
        didSet {
            updateColors()
        }
    }
    
    @IBInspectable var gradientStartColor: UIColor = .white {
        didSet {
            updateColors()
        }
    }
    
    @IBInspectable var gradientEndColor: UIColor = .black {
        didSet {
            updateColors()
        }
    }
    
    func updateColors()
    {
        if gradientLayer == nil {
            gradientLayer = CAGradientLayer()
            layer.masksToBounds = true
        }
        gradientLayer?.frame = bounds
        gradientLayer?.colors = [gradientStartColor.cgColor, gradientEndColor.cgColor]
        
        if isHorizontal {
            gradientLayer?.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradientLayer?.endPoint = CGPoint(x: 1.0, y: 0.5)
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        updateColors()
        layer.insertSublayer(gradientLayer!, at: 0)
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    func commonInit()
    {
        overlayView = UIView(frame:bounds)
        overlayView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        overlayView?.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        addSubview(overlayView!)
        overlayView?.isHidden = true
        
        addTarget(self, action: #selector(GradientButton.touchDown), for: .touchDown)
        addTarget(self, action: #selector(GradientButton.touchUp), for: .touchUpInside)
        addTarget(self, action: #selector(GradientButton.touchUp), for: .touchCancel)
        addTarget(self, action: #selector(GradientButton.touchUp), for: .touchDragExit)
        
        updateColors()
        
    }
    
    @objc func touchDown()
    {
        if let overlay = overlayView {
            bringSubviewToFront(overlay)
        }
        overlayView?.isHidden = false
    }
    
    @objc func touchUp()
    {
        overlayView?.isHidden = true
    }
    
    public override var isEnabled: Bool {
        didSet {
            
            if isEnabled {
                alpha = 1
            }
            else {
                alpha = 0.5
            }
        }
    }
}


