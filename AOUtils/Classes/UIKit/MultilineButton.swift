
//
//  MultilineButton.swift
//  Pods
//
//  Created by Flavien Simon on 13/07/2017.
//
//

import UIKit

open class MultilineButton: UIButton {
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.commonInit()
    }
    
    private func commonInit() {
        
        self.titleLabel?.numberOfLines = 0
        self.titleLabel?.lineBreakMode = .byWordWrapping
    }
    
    open override var intrinsicContentSize: CGSize {
        
        var size = titleLabel?.intrinsicContentSize
        size?.height += 10
        size?.width += 10
        return size ?? .zero
    }
    
    open override func layoutSubviews() {
        
        super.layoutSubviews()
        titleLabel?.preferredMaxLayoutWidth = titleLabel?.frame.size.width ?? 0
        super.layoutSubviews()
    }
    
    @IBInspectable var centerTitle: Bool {
        get { return false }
        set (value)
        {
            if value {
                titleLabel?.textAlignment = .center
            }
        }
    }
}
