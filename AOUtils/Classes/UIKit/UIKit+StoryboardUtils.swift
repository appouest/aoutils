//
//  Extensions.swift
//  Pods
//
//  Created by Aymeric De Abreu on 13/02/2017.
//
//

import Foundation
import UIKit

public extension UIView {
   
    @IBInspectable var borderWidth: Float {
        get {
            return Float(layer.borderWidth)
        }
        set (value) {
            layer.borderWidth = CGFloat(value)
        }
    }
    
    @IBInspectable var borderColor: UIColor {
        get {
            return UIColor.white
        }
        set (value) {
            layer.borderColor = value.cgColor
        }
    }
    
    @IBInspectable var cornerRadius: Float {
        get {
            return Float(layer.cornerRadius)
        }
        set (value) {
            layer.cornerRadius = CGFloat(value)
        }
    }
    
    @IBInspectable var shadowRadius: Float {
        get {
            return Float(layer.shadowRadius)
        }
        set (value) {
            layer.shadowRadius = CGFloat(value)
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return Float(layer.shadowOpacity)
        }
        set (value) {
            layer.shadowOpacity = value
        }
    }
    
    @IBInspectable var shadowOffsetZero: Bool {
        get {
            return true
        }
        set (value) {
            if value {
                layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            }
        }
    }
}

extension UIButton {
    
    @IBInspectable var adjusteFonSize: Bool {
        get { return false }
        set (value)
        {
            if value {
                titleLabel?.adjustsFontSizeToFitWidth = true
                titleLabel?.minimumScaleFactor = 0.5
            }
        }
    }
    
    @IBInspectable var useContentModeOnImage: Bool {
        get {
            return false
        }
        set(value) {
            if (value) {
                imageView?.contentMode = contentMode
            }
        }
    }
}

extension NSLayoutConstraint {
    
    @IBInspectable var isSeparator: Bool {
        get {
            return false
        }
        set (value) {
            constant = value ? 1 / UIScreen.main.scale : 1
        }
    }
}

extension UINavigationBar {
    
    @IBInspectable var removeShadow: Bool {
        set (key) {
            if key == true
            {
                shadowImage = UIImage()
                setBackgroundImage(UIImage(), for: .default)
            }
        }
        get {
            return false
        }
    }
    
    @IBInspectable var transparentBar: Bool {
        set (key) {
            if key == true
            {
                setBackgroundImage(UIImage(), for: UIBarMetrics.default)
                shadowImage = UIImage()
                isTranslucent = true
            }
        }
        get {
            return false
        }
    }
}

extension UISlider {
    
    @IBInspectable var thumbImageName: String? {
        set (key) {
            if let value = UIImage(named:key!) {
                setThumbImage(value, for: .normal)
            }
        }
        get {
            return nil
        }
    }
    
}

