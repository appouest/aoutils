//
//  CircularView.swift
//  Pods
//
//  Created by Flavien Simon on 18/07/2017.
//
//

import UIKit

open class CircularView: UIView {

    public override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.width / 2.0
        clipsToBounds = true
    }
}
