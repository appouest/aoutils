//
//  UIAlertController+Common.swift

//
//  Created by Aymeric De Abreu on 20/12/2016.
//  Copyright © 2016 Aymeric De Abreu. All rights reserved.
//

import Foundation
import UIKit

public extension UIAlertController {
    
    static func notImplemented(_ controller: UIViewController) {
        
        let alert = UIAlertController(title: "Not Implemented", message: "This feature is not implemented yet.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default))
        controller.present(alert, animated: true)
        
    }
    
    @objc static func showOkDialog(with title: String, message: String, controller: UIViewController, completion: (()->Void)? = nil)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("common.ok", comment: ""), style: UIAlertAction.Style.default, handler: { (action) in
            completion?()
        }))
        controller.present(alert, animated: true)
    }
}
