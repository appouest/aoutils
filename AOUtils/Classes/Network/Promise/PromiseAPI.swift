//
//  RxAPI.swift
//  AOUtils
//
//  Created by Aymeric De Abreu on 15/12/2017.
//

import Foundation
import PromiseKit


public extension APIClient {
    
    func data(resource: Resource, progress: TaskProgress? = nil) -> Promise<Data> {
        let request = resource.request(with: self.baseURL)
        return Request.shared.data(request: request, progress: progress)
    }
    
}
