//
//  RxRequest.swift
//  AOUtils
//
//  Created by Aymeric De Abreu on 15/12/2017.
//

import Foundation
import PromiseKit

public extension Request {
    
    func data(request: URLRequest, progress: TaskProgress? = nil) -> Promise<Data> {
        return Promise<Data> { seal in
            let _ = self.executeRequest(request: request, onSuccess: { data in
                seal.fulfill(data)
            }, onFailure: { (error) in
                seal.reject(error)
            }, progress: progress)
        }
    }
    
    func get(url: URL,
                    parameters: [String: Any]? = nil,
                    acceptType: MimeType? = nil,
                    progress: TaskProgress? = nil) -> Promise<Data> {
        let request = getRequest(url: url,
                                 parameters: parameters,
                                 acceptType: acceptType)
        return data(request: request, progress: progress)
    }
    
    func post(url: URL,
                     parameters: [String: Any]? = nil,
                     contentType: MimeType = .URLEncodedFormData,
                     acceptType: MimeType? = nil,
                     progress: TaskProgress? = nil) -> Promise<Data> {
        let request = postRequest(url: url,
                                  parameters: parameters,
                                  contentType: contentType,
                                  acceptType: acceptType)
        return data(request: request, progress: progress)
    }
    
    func postMultipart(url: URL,
                    parameters: [String: Any]? = nil,
                    multiparts: [String: MultipartData]? = nil,
                    acceptType: MimeType? = nil,
                    progress: TaskProgress? = nil) -> Promise<Data> {
        let request = postMultipartRequest(url: url,
                                           parameters: parameters,
                                           multiparts: multiparts,
                                           acceptType: acceptType)
        return data(request: request, progress: progress)
    }
    
    func put(url: URL,
                    parameters: [String: Any]? = nil,
                    contentType: MimeType = .URLEncodedFormData,
                    acceptType: MimeType? = nil,
                    progress: TaskProgress? = nil) -> Promise<Data> {
        let request = putRequest(url: url,
                                 parameters: parameters,
                                 contentType: contentType,
                                 acceptType: acceptType)
        return data(request: request, progress: progress)
    }
    
    func delete(url: URL,
                    parameters: [String: Any]? = nil,
                    acceptType: MimeType? = nil,
                    progress: TaskProgress? = nil) -> Promise<Data> {
        let request = deleteRequest(url: url,
                                    parameters: parameters,
                                    acceptType: acceptType)
        return data(request: request, progress: progress)
    }
}
