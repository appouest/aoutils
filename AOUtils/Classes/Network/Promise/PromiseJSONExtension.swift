//
//  RxJSONExtension.swift
//  AOUtils
//
//  Created by Aymeric De Abreu on 15/12/2017.
//

import Foundation
import SwiftyJSON
import PromiseKit

extension JSON {
    
    public static func toJSON(data: Data) -> Promise<JSON> {
        return Promise { seal in
            do {
                let json = try JSON(data: data)
                seal.fulfill(json)
            }
            catch {
                seal.reject(error)
            }
        }
    }
}
