//
//  RxResource.swift
//  AOUtils
//
//  Created by Aymeric De Abreu on 15/12/2017.
//

import Foundation
import PromiseKit

public extension Resource {
    func request(progress: TaskProgress? = nil) -> Promise<Data> {
        return APIClient.shared.data(resource: self, progress: progress)
    }
}
