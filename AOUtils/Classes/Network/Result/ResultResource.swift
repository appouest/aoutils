//
//  RxResource.swift
//  AOUtils
//
//  Created by Aymeric De Abreu on 15/12/2017.
//

import Foundation

public extension Resource {
    
    func request(progress: TaskProgress? = nil,
                 completion: @escaping (Result<Data, Error>) -> Void) {
        return APIClient.shared.data(resource: self, progress: progress, completion: completion)
    }
}
