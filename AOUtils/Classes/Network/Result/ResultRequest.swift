//
//  RxRequest.swift
//  AOUtils
//
//  Created by Aymeric De Abreu on 15/12/2017.
//

import Foundation

public extension Request {
    
    func data(request: URLRequest,
                     progress: TaskProgress? = nil,
                     completion: @escaping (Result<Data, Error>) -> Void) {
        let _ = executeRequest(request: request,
                               onSuccess: { (data) in
                                completion(.success(data))
        }, onFailure: {
            completion(.failure($0))
        }, progress: progress)
    }
    
    func get(url: URL,
                parameters: [String: Any]? = nil,
                acceptType: MimeType? = nil,
                progress: TaskProgress? = nil,
                completion: @escaping (Result<Data, Error>) -> Void) {
        let request = getRequest(url: url,
                                 parameters: parameters,
                                 acceptType: acceptType)
        data(request: request, progress: progress, completion: completion)
    }
    
    func post(url: URL,
                     parameters: [String: Any]? = nil,
                     contentType: MimeType = .URLEncodedFormData,
                     acceptType: MimeType? = nil,
                     progress: TaskProgress? = nil,
                     completion: @escaping (Result<Data, Error>) -> Void){
        let request = postRequest(url: url,
                                  parameters: parameters,
                                  contentType: contentType,
                                  acceptType: acceptType)
        data(request: request, progress: progress, completion: completion)
    }
    
    func postMultipart(url: URL,
                    parameters: [String: Any]? = nil,
                    multiparts: [String: MultipartData]? = nil,
                    acceptType: MimeType? = nil,
                    progress: TaskProgress? = nil,
                    completion: @escaping (Result<Data, Error>) -> Void) {
        let request = postMultipartRequest(url: url,
                                           parameters: parameters,
                                           multiparts: multiparts,
                                           acceptType: acceptType)
        data(request: request, progress: progress, completion: completion)
    }
    
    func put(url: URL,
                    parameters: [String: Any]? = nil,
                    contentType: MimeType = .URLEncodedFormData,
                    acceptType: MimeType? = nil,
                    progress: TaskProgress? = nil,
                    completion: @escaping (Result<Data, Error>) -> Void) {
        let request = putRequest(url: url,
                                 parameters: parameters,
                                 contentType: contentType,
                                 acceptType: acceptType)
        data(request: request, progress: progress, completion: completion)
    }
    
    func delete(url: URL,
                    parameters: [String: Any]? = nil,
                    acceptType: MimeType? = nil,
                    progress: TaskProgress? = nil,
                    completion: @escaping (Result<Data, Error>) -> Void) {
        let request = deleteRequest(url: url,
                                    parameters: parameters,
                                    acceptType: acceptType)
        data(request: request, progress: progress, completion: completion)
    }
}
