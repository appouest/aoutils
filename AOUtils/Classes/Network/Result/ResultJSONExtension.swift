//
//  RxJSONExtension.swift
//  AOUtils
//
//  Created by Aymeric De Abreu on 15/12/2017.
//

import Foundation
import SwiftyJSON

public extension JSON {
    
    static func toJSON(data: Data) -> Result<JSON, Error> {
        return Result { return try JSON(data: data) }
    }
}
