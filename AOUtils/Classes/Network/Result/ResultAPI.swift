//
//  RxAPI.swift
//  AOUtils
//
//  Created by Aymeric De Abreu on 15/12/2017.
//

import Foundation

public extension APIClient {
    
    func data(resource: Resource,
              progress: TaskProgress? = nil,
              completion: @escaping (Result<Data, Error>) -> Void) {
        var request = resource.request(with: self.baseURL)
        if resource.needAuth {
            if let challenge = authChallenge {
                challenge(&request)
            }
            else {
                print(lvl:.error, "Call need auth, but doesn't have a challege block")
            }
        }
        Request.shared.data(request: request, progress: progress, completion: completion)
    }
    
}
