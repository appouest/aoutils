//
//  Resource.swift
//  Alphorm
//
//  Created by Aymeric De Abreu on 25/01/2017.
//  Copyright © 2017 Aymeric De Abreu. All rights reserved.
//

import Foundation
import SwiftyJSON

public protocol Resource {
    
    var method: Method { get }
    var path: String { get }
    var needAuth: Bool { get }
    var parameters: [String: Any]? { get }
    var multiparts: [String: Any]? { get }
    var acceptType: MimeType { get }
    var contentType: MimeType { get }
    
}


public extension Resource {
    
    var needAuth: Bool {
        return false
    }
    
    var parameters: [String: Any]? {
        return nil
    }
    
    var multiparts: [String: Any]? {
        return nil
    }
    
    var contentType: MimeType {
        if let datas = multiparts, datas.count > 0 {
            return .MultipartFormData
        } else {
            return .URLEncodedFormData
        }
    }
    
    var acceptType: MimeType {
        return .JSON
    }
    
    var method: Method {
        return .GET
    }
    
    func request(with baseURL: URL) -> URLRequest {
        var bURL:URL? = baseURL
        if path.hasPrefix("http") {
            bURL = nil
        }
        guard let url = URL(string: path, relativeTo: bURL)?.absoluteURL else {
            fatalError("Unable to create URL Request from \(path)")
        }
        
        return Request.buildRequest(url: url,
                                    method: method,
                                    parameters: parameters,
                                    multipart: multiparts,
                                    acceptType: acceptType,
                                    contentType: contentType)
    }
}
