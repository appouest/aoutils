//
//  RxRequest.swift
//  AOUtils
//
//  Created by Aymeric De Abreu on 15/12/2017.
//

import Foundation
import RxSwift

public extension Request {
    
    
    func data(request: URLRequest) -> Observable<Data> {
        return Observable.create { observer in
            
            let task = self.executeRequest(request: request, onSuccess: { data in
                observer.onNext(data)
                observer.onCompleted()
            }, onFailure: { (error) in
                observer.onError(error)
            })
            
            return Disposables.create {
                task.cancel()
            }
        }.observeOn(MainScheduler.instance)
    }
}
