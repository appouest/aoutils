//
//  RxAPI.swift
//  AOUtils
//
//  Created by Aymeric De Abreu on 15/12/2017.
//

import Foundation
import RxSwift


public extension APIClient {
    
    func data(resource: Resource) -> Observable<Data> {
        var request = resource.request(with: self.baseURL)
        if resource.needAuth {
            if let challenge = authChallenge {
                challenge(&request)
            }
            else {
                print(lvl:.error, "Call need auth, but doesn't have a challege block")
            }
        }
        return Request.shared.data(request: request)
    }
    
}
