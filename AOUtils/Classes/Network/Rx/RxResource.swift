//
//  RxResource.swift
//  AOUtils
//
//  Created by Aymeric De Abreu on 15/12/2017.
//

import Foundation
import RxSwift

public extension Resource {
    func request() -> Observable<Data> {
        return APIClient.shared.data(resource: self)
    }
}
