//
//  RxJSONExtension.swift
//  AOUtils
//
//  Created by Aymeric De Abreu on 15/12/2017.
//

import Foundation
import SwiftyJSON
import RxSwift

public extension JSON {
    
    static func toJSON(data: Data) -> Observable<JSON> {
        return Observable.create { observer in
            do {
                let json = try JSON(data: data)
                observer.onNext(json)
                observer.onCompleted()
            }
            catch {
                observer.onError(error)
            }

            return Disposables.create { }
        }
    }
}
