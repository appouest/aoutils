//
//  QueryValue.swift
//  AOUtils
//
//  Created by Aymeric De Abreu on 08/06/2018.
//

import Foundation

protocol QueryValue {
    var queryValue: String { get }
}

extension Int: QueryValue {
    var queryValue: String {
        return "\(self)"
    }
}

extension Float: QueryValue {
    var queryValue: String {
        return "\(self)"
    }
}

extension Double: QueryValue {
    var queryValue: String {
        return "\(self)"
    }
}

extension Bool: QueryValue {
    var queryValue: String {
        return "\(self)"
    }
}

extension String: QueryValue {
    var queryValue: String {
        return self
    }
}



