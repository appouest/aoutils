//
//  Multipart.swift

//
//  Created by Aymeric De Abreu on 16/12/2016.
//  Copyright © 2016 Aymeric De Abreu. All rights reserved.
//


import Foundation
#if os(OSX)
    import CoreServices
#elseif os(iOS)
    import CoreServices
#endif

public struct MultipartData {
    let filename:String
    let data:Data
    let mimeType:String
    
    public init?(path:URL)
    {
        guard let data = try? Data(contentsOf: path) else { return nil }
        let filename = path.lastPathComponent
        self.init(filename:filename, data:data)
    }
    
    public init(filename:String, data: Data)
    {
        self.filename = filename
        self.data = data
        if let pathExtension = filename.components(separatedBy: ".").last ,
            let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue(),
            let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
            mimeType = mimetype as String
        }
        else {
            mimeType = "application/octet-stream"
        }
    }
    
}


public extension URLRequest {
    
    private func percentEscapeString(string: String) -> String {
        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(charactersIn: "-._* ")
        
        return string.addingPercentEncoding(withAllowedCharacters: characterSet)!
                     .replacingOccurrences(of: " ", with: "+")
    }
    
    
    mutating func buildBody(with parameters: [String: Any]?, datas: [String: Any]?) {
        if let datas = datas, datas.count > 0 {
            multipart(with: parameters, datas: datas)
        } else {
            formUrlEncoded(with: parameters)
        }
    }
    
    mutating func formUrlEncoded(with parameters: [String: Any]?) {
        setValue("\(MimeType.URLEncodedFormData.rawValue); charset=utf-8", forHTTPHeaderField: "Content-Type")
        if let params = parameters {
            let strParams = params.map({ (elt) -> String in
                if let value = elt.value as? QueryValue {
                    return "\(elt.key)=\(percentEscapeString(string: value.queryValue))"
                }
                else if let values = elt.value as? [QueryValue] {
                    let strValues = values.map { "\(elt.key)[]=\(percentEscapeString(string: $0.queryValue))" }
                    return strValues.joined(separator: "&")
                }
                else {
                    print(lvl: .debug, "Warning : \(elt.key) not a QueryValue")
                }
                return ""
            })
            httpBody = strParams.joined(separator: "&").data(using: .utf8)
        }
    }
    
    mutating func multipart(with parameters: [String: Any]?, datas: [String:Any]?)
    {
        let boundary = generateBoundaryString()
        setValue("\(MimeType.MultipartFormData.rawValue); boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        httpBody = createBody(with: parameters, datas: datas, boundary: boundary)
    }
    
    /// Create boundary string for multipart/form-data request
    ///
    /// - returns:            The boundary string that consists of "Boundary-" followed by a UUID string.
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    
    /// Create body of the multipart/form-data request
    ///
    /// - parameter parameters:   The optional dictionary containing keys and values to be passed to web service
    /// - parameter filePathKey:  The optional field name to be used when uploading files. If you supply paths, you must supply filePathKey, too.
    /// - parameter paths:        The optional array of file paths of the files to be uploaded
    /// - parameter boundary:     The multipart/form-data boundary
    ///
    /// - returns:                The NSData of the body of the request
    
    func createBody(with parameters: [String: Any]?, datas: [String: Any]?, boundary: String) -> Data {
        var body = Data()
        
        parameters?.forEach({ (key, value) in
            if let value = value as? QueryValue {
                body.append("--\(boundary)\r\n")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.append("\(value.queryValue)\r\n")
            }
            else if let value = value as? [QueryValue] {
                value.forEach({ (elt) in
                    body.append("--\(boundary)\r\n")
                    body.append("Content-Disposition: form-data; name=\"\(key)[]\"\r\n\r\n")
                    body.append("\(elt.queryValue)\r\n")
                })
            } else {
                print(lvl: .debug, "Warning : \(key) not a QueryValue")
            }
        })
        
        datas?.forEach({ (key, value) in
            if let multipartData = value as? MultipartData {
                body.append("--\(boundary)\r\n")
                body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(multipartData.filename)\"\r\n")
                body.append("Content-Type: \(multipartData.mimeType)\r\n\r\n")
                body.append(multipartData.data)
                body.append("\r\n")
            } else if let multipartDatas = value as? [MultipartData] {
                multipartDatas.forEach {
                    body.append("--\(boundary)\r\n")
                    body.append("Content-Disposition: form-data; name=\"\(key)[]\"; filename=\"\($0.filename)\"\r\n")
                    body.append("Content-Type: \($0.mimeType)\r\n\r\n")
                    body.append($0.data)
                    body.append("\r\n")
                }
            } else {
                print(lvl: .debug, "Warning : \(key) is not multipart")
            }
        })
        
        body.append("--\(boundary)--\r\n")
        return body
    }
    
    
}

public extension Data {
    
    /// Append string to NSMutableData
    ///
    /// Rather than littering my code with calls to `dataUsingEncoding` to convert strings to NSData, and then add that data to the NSMutableData, this wraps it in a nice convenient little extension to NSMutableData. This converts using UTF-8.
    ///
    /// - parameter string:       The string to be added to the `NSMutableData`.
    
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}







