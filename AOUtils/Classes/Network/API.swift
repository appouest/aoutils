//
//  API.swift

//
//  Created by Aymeric De Abreu on 16/12/2016.
//  Copyright © 2016 Aymeric De Abreu. All rights reserved.
//

import Foundation
import SwiftyJSON

public enum APIClientError: Error {
    case CouldNotDecodeJSON
    case ServerErrorMessage(message: String)
}

public enum ProgressType {
    case upload
    case download
}

public typealias TaskProgress = (ProgressType, Float) -> Void

public final class APIClient {
    
    public static var url: URL? {
        didSet {
            if let u = url {
                shared.baseURL = u
            }

        }
    }
    
    public var baseURL: URL
    
    public static var shared = APIClient(baseURL: url)
        
    init(baseURL: URL?) {
        if let baseURL = baseURL {
            self.baseURL = baseURL
        }
        else {
            fatalError("You should provide a correct url.")
        }
    }
    
    
    
    
}
