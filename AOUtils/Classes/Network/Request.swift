//
//  Request.swift
//  Alphorm
//
//  Created by Aymeric De Abreu on 25/01/2017.
//  Copyright © 2017 Aymeric De Abreu. All rights reserved.
//

import Foundation
import SwiftyJSON

public enum Method: String {
    case GET = "GET"
    case POST = "POST"
    case DELETE = "DELETE"
    case PUT = "PUT"
    case PATCH = "PATCH"
}

public enum MimeType: String {
    case URLEncodedFormData = "application/x-www-form-urlencoded"
    case MultipartFormData = "multipart/form-data"
    case JSON = "application/json"
    case XML = "text/xml"
}

public enum RequestError: Error {
    case BadURLRequest
    case BadStatus(status: Int, data: Data?)
    case Other(Error)
}

public enum RequestStatus {
    case success
    case error(error: Error)
}

public protocol RequestResult {
    var status: RequestStatus { get }
}

extension RequestResult {
    public var status: RequestStatus {
        return .success
    }
}

class RequestTask {
    
    let success: (Data) -> Void
    let failure: (Error) -> Void
    var progress: TaskProgress?
    
    var data: Data?
    var localURL: URL?
    
    init(success: @escaping (Data) -> Void, failure: @escaping (Error) -> Void) {
        self.success = success
        self.failure = failure
    }
    
    func appendData(_ someData: Data) {
        if data != nil {
            data!.append(someData)
        } else {
            data = someData
        }
    }
    
}

public protocol RequestAdapter {
    func adapt(_ request: inout URLRequest)
    func beforeSend(_ request: URLRequest)
    func onResponse(response: URLResponse?, data: Data?)
    func onError(request: URLRequest?, error: Error)
    func onSuccess(request: URLRequest?)
}

public extension RequestAdapter {
    func adapt(_ request: inout URLRequest) { }
    func beforeSend(_ request: URLRequest) { }
    func onResponse(response: URLResponse?, data: Data?) { }
    func onError(request: URLRequest?, error: Error) { }
    func onSuccess(request: URLRequest?) { }
}


public class Request: NSObject {
    
    public static var shared = Request()
    
    var session: URLSession?
    
    var tasks: [Int: RequestTask] = [:]
    private var adapters: [RequestAdapter] = []
    
    public init(configuration: URLSessionConfiguration = URLSessionConfiguration.default, adapters: [RequestAdapter] = []) {
        super.init()
        self.session = URLSession(configuration: configuration,
                                  delegate: self,
                                  delegateQueue: OperationQueue.main)
        self.adapters = adapters
        
    }
    
    public func getRequest(url: URL,
                           parameters: [String: Any]? = nil,
                           acceptType: MimeType? = nil) -> URLRequest {
        return Request.buildRequest(url: url,
                                           method: .GET,
                                           parameters: parameters,
                                           multipart: nil,
                                           acceptType: nil,
                                           contentType: .URLEncodedFormData)
    }
    
    public func postRequest(url: URL,
                            parameters: [String: Any]? = nil,
                            contentType: MimeType = .URLEncodedFormData,
                            acceptType: MimeType? = nil) -> URLRequest {
        return Request.buildRequest(url: url,
                                           method: .POST,
                                           parameters: parameters,
                                           multipart: nil,
                                           acceptType: acceptType,
                                           contentType: contentType)
    }
    
    public func postMultipartRequest(url: URL,
                                     parameters: [String: Any]? = nil,
                                     multiparts: [String: Any]? = nil,
                                     acceptType: MimeType? = nil) -> URLRequest {
        return Request.buildRequest(url: url,
                                           method: .POST,
                                           parameters: parameters,
                                           multipart: multiparts,
                                           acceptType: acceptType,
                                           contentType: .MultipartFormData)
    }
    
    public func putRequest(url: URL,
                     parameters: [String: Any]? = nil,
                     contentType: MimeType = .URLEncodedFormData,
                     acceptType: MimeType? = nil) -> URLRequest {
        return Request.buildRequest(url: url,
                                           method: .PUT,
                                           parameters: parameters,
                                           multipart: nil,
                                           acceptType: nil,
                                           contentType: contentType)
    }
    
    public func deleteRequest(url: URL,
                     parameters: [String: Any]? = nil,
                     acceptType: MimeType? = nil) -> URLRequest {
        return Request.buildRequest(url: url,
                                           method: .DELETE,
                                           parameters: parameters,
                                           multipart: nil,
                                           acceptType: nil,
                                           contentType: .URLEncodedFormData)
    }
    
    
    public func executeRequest(request: URLRequest,
                               onSuccess: @escaping ((Data)->Void),
                               onFailure: @escaping ((Error)->Void),
                               progress: TaskProgress? = nil) -> URLSessionDataTask? {
        
        print(lvl: .info, "[\(request.httpMethod ?? "")] \(request.url?.absoluteString ?? "")")
        var localRequest = request
        
        self.adapters.forEach { $0.adapt(&localRequest) }
        
        var sessionTask: URLSessionDataTask?
        let requestTask = RequestTask(success: onSuccess,
                                      failure: onFailure)
        requestTask.progress = progress
        
        if let data = localRequest.httpBody {
            if let session = session {
                //Check if we are in background
                if session.configuration.identifier == nil {
                    sessionTask = session.uploadTask(with: localRequest, from: data)
                } else {
                    print(lvl: .info, "In background, will use file for upload")
                    let tempDir: URL
                    if #available(iOS 10.0, *) {
                        tempDir = FileManager.default.temporaryDirectory
                    } else {
                        tempDir = URL(fileURLWithPath: NSTemporaryDirectory())
                    }
                    let localURL = tempDir.appendingPathComponent(UUID().uuidString)
                    do {
                        try data.write(to: localURL)
                        requestTask.localURL = localURL
                        sessionTask = session.uploadTask(with: localRequest, fromFile: localURL)
                    } catch {
                        onFailure(error)
                        return nil
                    }
                }
            }
            
        } else {
            sessionTask = session?.dataTask(with: localRequest)
        }
        
        guard let task = sessionTask else {
            fatalError("Error while creating task")
        }
        
        tasks[task.taskIdentifier] = requestTask
        
        self.adapters.forEach { $0.beforeSend(localRequest) }
        task.resume()
        
        return task
    }
    
    public static func buildRequest(url: URL,
                                    method: AOUtils.Method,
                                    parameters: [String: Any]?,
                                    multipart: [String: Any]?,
                                    acceptType: MimeType?,
                                    contentType: MimeType) -> URLRequest {
        guard var components = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            fatalError("Unable to create URL components from \(url)")
        }
        if(method == .GET || method == .DELETE)
        {
            if let params = parameters?.flatMap({ (elt) -> [URLQueryItem] in
                if let value = elt.value as? QueryValue {
                    return [URLQueryItem(name: String(elt.key), value: value.queryValue)]
                }
                else if let values = elt.value as? [QueryValue] {
                    var items = [URLQueryItem]()
                    for value in values {
                        items.append(URLQueryItem(name: "\(elt.key)[]", value: value.queryValue))
                    }
                    return items
                }
                return []
            }){
                if components.queryItems != nil {
                    components.queryItems?.append(contentsOf: params)
                }
                else
                {
                    components.queryItems = params
                }
            }
            
        }
        
        guard let finalURL = components.url else {
            fatalError("Unable to retrieve final URL")
        }
        
        var request = URLRequest(url: finalURL)
        if let acceptType = acceptType {
            request.setValue(acceptType.rawValue,
                             forHTTPHeaderField: "Accept")
        }
        
        request.httpMethod = method.rawValue
        
        if (method == .POST || method == .PUT || method == .PATCH)
        {
            switch contentType {
            case .URLEncodedFormData:
                request.formUrlEncoded(with: parameters)
            case .MultipartFormData:
                request.multipart(with: parameters, datas: multipart)
            case .JSON:
                request.setValue(contentType.rawValue, forHTTPHeaderField: "Content-Type")
                if let params = parameters {
                    let json = JSON(params)
                    if let jsonStr = json.rawString(.utf8),
                        let data = jsonStr.data(using: .utf8) {
                        request.httpBody = data
                    }
                }
            default:
                break
            }
            
        }
        
        return request
    }
    
}

extension Request: URLSessionDelegate, URLSessionTaskDelegate, URLSessionDataDelegate {
    
    public func urlSession(_ session: URLSession,
                           task: URLSessionTask,
                           didCompleteWithError error: Error?) {
        guard let requestTask = tasks[task.taskIdentifier] else { return }
        
        if let error = error {
            
            self.adapters.forEach { $0.onError(request: task.originalRequest,
                                               error: error) }
            
            requestTask.failure(RequestError.Other(error))
        } else {
            adapters.forEach { $0.onResponse(response: task.response,
                                             data: requestTask.data) }
            
            guard let HTTPResponse = task.response as? HTTPURLResponse else {
                fatalError("Couldn't get HTTP response")
            }
            
            if 200 ..< 300 ~= HTTPResponse.statusCode {
                print(lvl: .info, "Request OK, status code : \(HTTPResponse.statusCode)")
                
                self.adapters.forEach { $0.onSuccess(request: task.originalRequest) }
                tasks[task.taskIdentifier]?.success(requestTask.data ?? Data())
            }
            else {
                print(lvl: .error, "Request error, status code : \(HTTPResponse.statusCode)")
                if let data = requestTask.data,
                    let errorStr = String(data: data, encoding: .utf8) {
                    print(lvl: .error, "Error returned : \n\(errorStr)")
                }
                let error = RequestError.BadStatus(status: HTTPResponse.statusCode,
                                                   data: requestTask.data)
                    
                
                self.adapters.forEach { $0.onError(request: task.originalRequest,
                                                   error: error) }
                
                requestTask.failure(error)
            }
        }
        
        tasks.removeValue(forKey: task.taskIdentifier)
        if let url = requestTask.localURL {
            try? FileManager.default.removeItem(at: url)
        }
    }
    
    public func urlSession(_ session: URLSession,
                           dataTask: URLSessionDataTask,
                           didReceive data: Data) {
        guard let requestTask = tasks[dataTask.taskIdentifier] else { return }
        requestTask.appendData(data)
        if let buffer = requestTask.data,
            dataTask.countOfBytesExpectedToReceive > 0 {
            let percent = Float(buffer.count) / Float(dataTask.countOfBytesExpectedToReceive)
            requestTask.progress?(.download, percent)
        }
        
    }
    
    public func urlSession(_ session: URLSession,
                           task: URLSessionTask,
                           didSendBodyData bytesSent: Int64,
                           totalBytesSent: Int64,
                           totalBytesExpectedToSend: Int64) {
        guard let requestTask = tasks[task.taskIdentifier],
            let progress = requestTask.progress else { return }
        let uploadProgress = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
        progress(.upload, uploadProgress)
    }
    
}

