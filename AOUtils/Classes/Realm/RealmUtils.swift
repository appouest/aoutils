//
//  RealmUtils.swift

//
//  Created by Aymeric De Abreu on 26/12/2016.
//  Copyright © 2016 Aymeric De Abreu. All rights reserved.
//

import Foundation
import RealmSwift

public extension List {
    func sync<S: Sequence>(objectsIn objects: S) where S.Iterator.Element == Element {
        removeAll()
        append(objectsIn: objects)
    }
    
    func appendIfNotIn(_ object: Element)
    {
        let objectIndex = index(of: object)
        if objectIndex == nil {
            append(object)
        }
    }
    func appendIfNotIn<S: Sequence>(objectsIn objects: S) where S.Iterator.Element == Element
    {
        for obj in objects {
            appendIfNotIn(obj)
        }
    }
}

public extension Realm {
    static func write(_ block:(_ realm:Realm)->Void) {
        do {
            let realm = try Realm()
            
            try realm.write {
                block(realm)
            }
            
        }
        catch {}
    }
}


public protocol PrimaryObj {
    var identifier: String { get set }
}

public extension PrimaryObj where Self : Object{
    static func findFirstOrCreate(withId id:String, realm: Realm) -> Self {
        var object: Self
        if let a = realm.object(ofType: Self.self, forPrimaryKey: id)
        {
            object = a
        }
        else
        {
            object = Self()
            object.identifier = id
            realm.add(object)
        }
        return object
    }
}

