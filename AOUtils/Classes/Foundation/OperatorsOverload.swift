//
//  OperatorsOverload.swift
//  AOUtils
//
//  Created by Jean-Baptiste Denoual on 14/12/2017.
//

import Foundation

func - (lhs:CGFloat, rhs:Int) -> CGFloat {
    return lhs - CGFloat(rhs)
}

func - (lhs:Int, rhs:CGFloat) -> CGFloat {
    return CGFloat(lhs) - rhs
}

func < (lhs:CGFloat, rhs:Int) -> Bool {
    return lhs < CGFloat(rhs)
}

func < (lhs:Int, rhs:CGFloat) -> Bool {
    return CGFloat(lhs) < rhs
}

func > (lhs:CGFloat, rhs:Int) -> Bool {
    return lhs > CGFloat(rhs)
}

func > (lhs:Int, rhs:CGFloat) -> Bool {
    return CGFloat(lhs) > rhs
}

func * (lhs:CGFloat, rhs:Double) -> CGFloat {
    return lhs * CGFloat(rhs)
}

func * (lhs:Double, rhs:CGFloat) -> CGFloat {
    return CGFloat(lhs) * rhs
}

func * (lhs:CGFloat, rhs:Int) -> CGFloat {
    return lhs * CGFloat(rhs)
}

func * (lhs:Int, rhs:CGFloat) -> CGFloat {
    return CGFloat(lhs) * rhs
}

func * (lhs:CGFloat, rhs:Float) -> CGFloat {
    return lhs * CGFloat(rhs)
}

func * (lhs:Float, rhs:CGFloat) -> CGFloat {
    return CGFloat(lhs) * rhs
}

func / (lhs:CGFloat, rhs:Double) -> CGFloat {
    return lhs / CGFloat(rhs)
}

func / (lhs:Double, rhs:CGFloat) -> CGFloat {
    return CGFloat(lhs) / rhs
}

func / (lhs:CGFloat, rhs:Int) -> CGFloat {
    return lhs / CGFloat(rhs)
}

func / (lhs:Int, rhs:CGFloat) -> CGFloat {
    return CGFloat(lhs) / rhs
}

func / (lhs:CGFloat, rhs:Float) -> CGFloat {
    return lhs / CGFloat(rhs)
}

func / (lhs:Float, rhs:CGFloat) -> CGFloat {
    return CGFloat(lhs) / rhs
}

func - (lhs: CGPoint, rhs: CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
}

public func * (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x * right.x, y: left.y * right.y)
}

public func * (point: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: point.x * scalar, y: point.y * scalar)
}

public func * (point: CGPoint, scalar: Float) -> CGPoint {
    return CGPoint(x: point.x * CGFloat(scalar), y: point.y * CGFloat(scalar))
}

public func + (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

