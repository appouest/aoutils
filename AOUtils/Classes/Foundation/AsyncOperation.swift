//
//  AsyncOperation.swift
//  Pods
//
//  Created by Aymeric De Abreu on 10/03/2017.
//
//

import Foundation

class AsyncOperation: Operation {
    
    enum State {
        case ready, executing, finished, cancelled
        
        func keyPath() -> String {
            switch self {
            case .ready:
                return "isReady"
            case .executing:
                return "isExecuting"
            case .finished:
                return "isFinished"
            case .cancelled:
                return "isCancelled"
            }
        }
    }
    
    // MARK: - Properties
    
    var state = State.ready {
        willSet {
            willChangeValue(forKey: newValue.keyPath())
            willChangeValue(forKey: state.keyPath())
        }
        didSet {
            didChangeValue(forKey: oldValue.keyPath())
            didChangeValue(forKey: state.keyPath())
        }
    }
    
    // MARK: - NSOperation
    
    override var isReady: Bool {
        return super.isReady && state == .ready
    }
    
    override var isExecuting: Bool {
        return state == .executing
    }
    
    override var isFinished: Bool {
        return state == .finished
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    override func cancel() {
        state = .cancelled
    }
    
    override func start() {
        if state == .cancelled {
            state = .finished
            return
        }
        
        fatalError("AsyncOperation should override start function")
    }
}
