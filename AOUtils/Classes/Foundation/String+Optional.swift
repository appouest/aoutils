//
//  String+Optional.swift
//  AOUtils
//
//  Created by Jean-Baptiste Denoual on 14/12/2017.
//

import Foundation

extension Optional where Wrapped == String {
    var nilIfEmpty: String? {
        guard let strongSelf = self else {
            return nil
        }
        return strongSelf.isEmpty ? nil : strongSelf
    }
}
