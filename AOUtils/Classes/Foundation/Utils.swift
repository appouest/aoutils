//
//  Utils.swift
//  Pods
//
//  Created by Aymeric De Abreu on 13/02/2017.
//
//

import Foundation

public enum LogLevel : Int {
    case all = 0
    case info
    case debug
    case error
}

public func print(lvl level: LogLevel,_ items: Any...)
{
    if Utils.logLevel.rawValue <= level.rawValue {
        print("[AOUtils] ", terminator: "")
        for i in items {
            print(i, terminator:"")
        }
        print("")
    }
}

extension Collection {
    
    /// Returns the element at the specified index iff it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

public struct Utils {
    
    public static var logLevel = LogLevel.all
    
    public static var documentPath : String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        return paths.first ?? ""
    }
    
    public static var tmpFolderPath : String {
        return NSTemporaryDirectory()
    }
}

public struct Platform {
    public static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
            isSim = true
        #endif
        return isSim
    }()
}

