//
//  UserDefaults.swift

//
//  Created by Aymeric De Abreu on 21/12/2016.
//  Copyright © 2016 Aymeric De Abreu. All rights reserved.
//

import Foundation

public protocol BoolDefaultSettable : KeyNamespaceable {
    associatedtype BoolKey : RawRepresentable
}

public extension BoolDefaultSettable where BoolKey.RawValue == String {
    
    func set(_ value: Bool, forKey key: BoolKey) {
        let key = key.rawValue
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func bool(forKey key: BoolKey) -> Bool {
        let key = key.rawValue
        return UserDefaults.standard.bool(forKey: key)
    }
    
    subscript(_ key: BoolKey) -> Bool {
        return bool(forKey: key)
    }
}

public protocol IntegerDefaultSettable : KeyNamespaceable{
    associatedtype IntKey : RawRepresentable
}

public extension IntegerDefaultSettable where IntKey.RawValue == String {
    
    func set(_ value: Int, forKey key: IntKey) {
        let key = namespaced(key)
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func integer(forKey key: IntKey) -> Int {
        let key = namespaced(key)
        return UserDefaults.standard.integer(forKey: key)
    }
    
    subscript(_ key: IntKey) -> Int {
        return integer(forKey: key)
    }
}

public protocol StringDefaultSettable : KeyNamespaceable{
    associatedtype StringKey : RawRepresentable
}

public extension StringDefaultSettable where StringKey.RawValue == String {
    
    func set(_ value: String?, forKey key: StringKey) {
        let key = namespaced(key)
        if let value = value {
            UserDefaults.standard.set(value, forKey: key)
        }
        else {
            UserDefaults.standard.removeObject(forKey: key)
        }
    }
    
    func string(forKey key: StringKey) -> String? {
        let key = namespaced(key)
        return UserDefaults.standard.string(forKey: key)
    }
    
    subscript(_ key: StringKey) -> String? {
        get {
            return string(forKey: key)
        }
        set (value) {
            set(value, forKey: key)
        }
    }
}

public protocol ObjectDefaultSettable : KeyNamespaceable{
    associatedtype ObjectKey : RawRepresentable
}

public extension ObjectDefaultSettable where ObjectKey.RawValue == String {
    
    func set(_ value: Any?, forKey key: ObjectKey) {
        let key = namespaced(key)
        if let value = value {
            UserDefaults.standard.set(value, forKey: key)
        }
        else {
            UserDefaults.standard.removeObject(forKey: key)
        }
    }
    
    func object(forKey key: ObjectKey) -> Any? {
        let key = namespaced(key)
        return UserDefaults.standard.object(forKey: key)
    }
    
    subscript(_ key: ObjectKey) -> Any? {
        get {
            return object(forKey: key)
        }
        set (value) {
            set(value, forKey: key)
        }
    }
}

public protocol KeyNamespaceable {
    func namespaced<T: RawRepresentable>(_ key: T) -> String
}

public extension KeyNamespaceable {
    func namespaced<T: RawRepresentable>(_ key: T) -> String {
        return "\(Self.self).\(key.rawValue)"
    }
}

public extension UserDefaults {
    static var `default` : UserDefaults {
        return standard
    }
}


//protocol DoubleDefaultSettable { ... }
//
//protocol FloatDefaultSettable { ... }
//
//protocol ObjectDefaultSettable { ... }
//
//protocol URLDefaultSettable { ... }
