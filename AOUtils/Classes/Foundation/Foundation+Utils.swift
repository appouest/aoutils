//
//  FoundationExtensions.swift
//  Pods
//
//  Created by Aymeric De Abreu on 13/02/2017.
//
//

import Foundation

public extension String {
    
   var lastPathComponent: String {
        get {
            return (self as NSString).lastPathComponent
        }
    }
    
    var pathExtension: String {
        get {
            
            return (self as NSString).pathExtension
        }
    }
    
    var deletingLastPathComponent: String {
        get {
            
            return (self as NSString).deletingLastPathComponent
        }
    }
    
    var deletingPathExtension: String {
        get {
            
            return (self as NSString).deletingPathExtension
        }
    }
    
    var pathComponents: [String] {
        get {
            
            return (self as NSString).pathComponents
        }
    }
    
    func appendingPathComponent(path: String) -> String {
        
        let nsSt = self as NSString
        
        return nsSt.appendingPathComponent(path)
    }
    
    func appendingPathExtension(ext: String) -> String? {
        
        let nsSt = self as NSString
        
        return nsSt.appendingPathExtension(ext)
    }

}

public extension DateFormatter {
    
    static var defaultTimeZone: TimeZone? {
        
        didSet {
            if let timeZone = defaultTimeZone {
                sqlDate.timeZone = timeZone
                sqlDateTime.timeZone = timeZone
            }
            else {
                sqlDateTime.timeZone = TimeZone(secondsFromGMT: 0)
                sqlDate.timeZone = TimeZone(secondsFromGMT: 0)
            }
        }
    }
    
    static let sqlDateTime : DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()
    
    static let sqlDate : DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
}

public extension URL {
    
    init?(withFilename name:String, inDirectory directory: String = Utils.documentPath) {
        let filepath = "\(directory)/\(name)"
        self.init(fileURLWithPath: filepath)
    }
}

public extension Data {
    
    func writeWithName(_ name: String, inDirectory directory: String = Utils.documentPath) -> URL? {
        let filepath = "\(directory)/\(name)"
        let url = URL(fileURLWithPath: filepath)
        do {
            try write(to: url)
        }
        catch {
            print(lvl: .error, "error writing file : \(error)")
        }
        return url
    }
    
}

public extension TimeInterval {
    
    var durationTimeFormat:String {
        let minutes = Int(self/60)
        let secondsLeft = Int(self.truncatingRemainder(dividingBy: 60))
        return String(format: "%02d:%02d", minutes, secondsLeft)
    }
}
